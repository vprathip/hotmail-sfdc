public class AccountUtils {
    public static List<Account> accountsByState(String statecode){
        List<Account> acctList = [select id,name from account where billingstate = :statecode];
        return acctList;
    }
}