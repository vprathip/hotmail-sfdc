public with sharing class customattachment {

  public Customattachment__c ca {get; set;} 
  public String attachmentURL;

  public customattachment(ApexPages.StandardController controller) {
    ca = (Customattachment__c)controller.getRecord();
    }
    
  public Attachment attachment {
  get {
      if (attachment == null)
        attachment = new Attachment();
      return attachment;
    }
  set;
  }
 
  public PageReference Savecustom() {
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = ca.parentobject__c; // the record the file is attached to
    attachment.IsPrivate = true;
    insert attachment;
    ca.attid__c = attachment.id;
    attachmentURL = 'https://na14.salesforce.com/' + ca.attid__c;
    ca.attachment_link__c = attachmentURL;
    insert ca;
    PageReference acctPage = new ApexPages.StandardController(ca).view();
    acctPage.setRedirect(true);
    return acctPage;    
  }

}