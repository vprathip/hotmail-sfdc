public class AccountHandler {
    public static Account insertNewAccount(String acctName){
        Account acct = new Account();
        acct.name = acctName;
        try{
            insert acct;
            return acct;
        }catch(DmlException e){
            System.debug('A DML exception has occurred: ' + e.getMessage());
            return null;
        }
    }
}