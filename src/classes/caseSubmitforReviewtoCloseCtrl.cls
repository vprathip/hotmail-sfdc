public class caseSubmitforReviewtoCloseCtrl {
  
    @AuraEnabled
    public static void updateCase(Id caseId) {
        Case currentcase = [SELECT Id, subject FROM Case where id = :caseId];
        currentcase.status = 'Working';
        update currentcase;
        system.debug('current case = ' +currentcase);
    }   
    
    @AuraEnabled
    public static case getCase(Id caseId1) {
        Case case1 = [SELECT priority, subject, status FROM Case where id = :caseId1];
        return case1;
    }             
 }