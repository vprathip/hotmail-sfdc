public class ContactAndLeadSearch {
    public static List<List<SObject>> searchContactsAndLeads(String searchText){
        List<List< SObject>> conLeadList= new List<List<SObject>>();
        conLeadList = [FIND :searchText IN NAME FIELDS RETURNING Contact, Lead];
        return conLeadList;
    }
}