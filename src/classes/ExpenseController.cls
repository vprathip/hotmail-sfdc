public with sharing class ExpenseController {
    @AuraEnabled
    public static List<Expense__c> findExpenses() {
        return [SELECT Id, Name, Amount__c, Client__c, Date__c,
        Reimbursed__c, CreatedDate FROM Expense__c ];
    }
    
	@AuraEnabled
    public static List<Expense__c> getExpenses(String expense, String amount) {
        Integer amt = Integer.valueOf(amount);
        return [SELECT Id, Name, Amount__c, Client__c, Date__c, Reimbursed__c, CreatedDate 
                FROM Expense__c 
                where Name = :expense and amount__c = :amt];
    }
}