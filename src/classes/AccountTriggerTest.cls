@isTest
public class AccountTriggerTest {
    public static testMethod void testShippingAddress() {
        List<Account> accts = new List<Account>();
        for (Integer i=0; i <200; i++){
            Account acct = new Account();
            acct.name = 'Test ' + i;
            acct.BillingState = 'CA';
            accts.add(acct);
        }
        test.startTest();
        insert accts;
        List<Account> acts = [select id, BillingState, ShippingState from account];
        for(Account act:acts){
            System.assertEquals(act.ShippingState, 'CA');
        }
        test.stopTest();
    }
}