public class ContactSearch {
    public static List<Contact> searchForContacts(String LName, string zCode){
        return [select id, name from contact where LastName =:LName and MailingPostalCode =:zCode];
    }
}