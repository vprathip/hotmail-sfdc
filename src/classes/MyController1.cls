public with sharing class MyController1 {

    public Account account { get; set; }
    
    public MyController1() {
       account = [select name from account where id =:Apexpages.currentpage().getParameters().get('id') ];
        
    }
    
    public PageReference Save() { 
    update account;
    return null;
    }
    
}