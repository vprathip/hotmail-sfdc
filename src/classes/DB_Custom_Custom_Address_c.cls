@isTest private without sharing class DB_Custom_Custom_Address_c
{
	@isTest (SeeAllData=true)
	private static void testTrigger()
	{
		CRMfusionDBR101.DB_Globals.triggersDisabled = true;
		sObject testData = CRMfusionDBR101.DB_TriggerHandler.createTestData( Custom_Address__c.getSObjectType() );
		Test.startTest();
		insert testData;
		update testData;
		CRMfusionDBR101.DB_Globals.generateCustomTriggerException = true;
		update testData;
		CRMfusionDBR101.DB_Globals.generateCustomTriggerException = false;
		delete testData;
		Test.stopTest();
	}
}