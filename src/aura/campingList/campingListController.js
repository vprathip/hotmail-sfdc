({
    doInit: function(component, event, helper) {
        var action1 = component.get("c.getItems");
        var action2 = component.get("c.getContacts");      
        
        action1.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.items", response.getReturnValue());
				$A.enqueueAction(action2);
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        
        action2.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.contacts", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action1);        
    },

    handleAddItem: function(component, event, helper) {
        	var item = event.getParam("item");
    		var action = component.get("c.saveItem");
    		action.setParams({"item": item});
    		action.setCallback(this, function(response){
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                        // all good, nothing to do.
                    var items = component.get("v.items");
                    items.push(response.getReturnValue());
                    component.set("v.items", items);
                }
    		});
    		$A.enqueueAction(action);
     }    

})