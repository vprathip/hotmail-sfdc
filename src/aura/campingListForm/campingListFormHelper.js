({
        createItem: function(component, event, helper) {
        var item = component.get("v.newItem");
        var addItem = component.getEvent("addItem");
        addItem.setParams({ "item": item });
        addItem.fire();
        component.set("v.newItem",{'sObjectType':'Camping_Item__c','Name':'','Quantity__c':0,'Price__c':0,'Packed__c':false})            
    }     
})