({
    doInit : function(component, event, helper) {
        var mydate = component.get("v.expense2.Date__c");
        if(mydate){
            component.set("v.formatdate", new Date(mydate));
        }
    },
    
    clickReimbursed: function(component, event, helper) {
        var expense3 = component.get("v.expense2");
        var updateEvent = component.getEvent("updateExpense");
        updateEvent.setParams({ "expense": expense3 });
        updateEvent.fire();
    }  

})