({
    
    doInit: function(component, event, helper) {   
        var action = component.get("c.getCases");
		action.setParams({caseId1:component.get("v.recordId")});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.cases", response.getReturnValue());
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    },
    
    myAction : function(component, event, helper) {
        var subj = component.get("v.cases[0].Subject");
        alert('subjectx = ');
        alert(subj);
        helper.updateCase(component);
    }
})