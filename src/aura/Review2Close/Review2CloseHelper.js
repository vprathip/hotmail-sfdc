({
	updateCase : function(component, event, helper) {
       var action = component.get("c.updateCase");
       action.setParams({caseId:component.get("v.recordId")});
       action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                alert('Case has been updated.  Please refresh the browser to see the details.');
            }
        });
        $A.enqueueAction(action);
    },         
})