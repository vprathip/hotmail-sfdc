({
	doInit : function(component, event, helper) {
		var timezone = $A.get("$Locale.timezone");
        alert('Time Zone Preference in Salesforce ORG :'+timezone);
        var mydate = new Date().toLocaleString("en-US", {timeZone: timezone})
        alert('Date Instance with Salesforce Locale timezone : '+mydate);
        var mydate1 = new Date().toISOString();
        alert('mydate1 ='+mydate1);
	}
})