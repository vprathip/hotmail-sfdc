({
    findExpenses: function(component, event, helper) {
        var action = component.get("c.getExpenses");
		var exp1 = component.get("v.newExpense");
		action.setParams({
            "expense": exp1.Name,
			"amount": exp1.Amount__c
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.expenses", response.getReturnValue());
                component.set("v.buttondisabled", "true");                
            }
            else {
                console.log("Failed with state: " + state);
            }
        });
        $A.enqueueAction(action);
    }
})