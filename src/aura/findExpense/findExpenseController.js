({
    findExpense: function(component, event, helper) {
            var newExpense = component.get("v.newExpense");
            helper.findExpenses(component, newExpense);
    },
    
    expenseNameChanged : function(component, event, helper) {
			component.set("v.buttondisabled", "false");         
    }, 
})