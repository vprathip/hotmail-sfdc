<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ACP_Flag_is_turned_on</fullName>
        <ccEmails>vramana@azdes.gov</ccEmails>
        <description>ACP Flag is turned on</description>
        <protected>false</protected>
        <recipients>
            <recipient>pv_ramana2@hotmail.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pv_ramana6@hotmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ACP_Flag_in_Turned_on</template>
    </alerts>
    <fieldUpdates>
        <fullName>update_SSN</fullName>
        <field>SSN__c</field>
        <formula>IF( ISNUMBER(SSN__c) ,  &apos;LEFT(SSN__c, 3) -  RIGHT(SSN__c, 4)&apos; , &apos;0&apos;)</formula>
        <name>update SSN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ACP Flag in Turned on</fullName>
        <actions>
            <name>ACP_Flag_is_turned_on</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.ACP_Flag__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SSN formating</fullName>
        <actions>
            <name>update_SSN</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.SSN__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
