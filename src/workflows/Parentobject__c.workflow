<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>td_email</fullName>
        <description>td email</description>
        <protected>false</protected>
        <recipients>
            <recipient>pv_ramana2@hotmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MarketingProductInquiryResponse</template>
    </alerts>
    <fieldUpdates>
        <fullName>Quantity_update</fullName>
        <field>qty__c</field>
        <formula>qty__c + 100</formula>
        <name>Quantity update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Approved</fullName>
        <field>Status__c</field>
        <formula>&quot;Approved&quot;</formula>
        <name>Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Open</fullName>
        <field>Status__c</field>
        <formula>&quot;Open&quot;</formula>
        <name>Status Open</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Rejected</fullName>
        <field>Status__c</field>
        <formula>&quot;Rejected&quot;</formula>
        <name>Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Step_1_approved</fullName>
        <field>Status__c</field>
        <formula>&quot;Step 1 Approved&quot;</formula>
        <name>Status - Step 1 approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Step1_Rejected</fullName>
        <field>Status__c</field>
        <formula>&quot;Step 1 Rejected&quot;</formula>
        <name>Step1 Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>status_pending</fullName>
        <field>Status__c</field>
        <formula>&quot;Pending&quot;</formula>
        <name>status pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>td_field_upd</fullName>
        <field>Name</field>
        <formula>Name + &apos; a&apos;</formula>
        <name>td field upd</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>merchandise wf rule</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Parentobject__c.qty__c</field>
            <operation>greaterThan</operation>
            <value>20</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Test_Merchandise_WF_rule</name>
                <type>Task</type>
            </actions>
            <timeLength>30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Test_Merchandise_WF_rule</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>2</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Test Merchandise WF rule</subject>
    </tasks>
</Workflow>
