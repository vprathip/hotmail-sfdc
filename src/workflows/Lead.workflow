<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>X24_hr_notice</fullName>
        <description>24 hr notice</description>
        <protected>false</protected>
        <recipients>
            <recipient>pv_ramana2@hotmail.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pv_ramana6@hotmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CommunityWelcomeEmailTemplate</template>
    </alerts>
    <alerts>
        <fullName>multi_language_template</fullName>
        <description>multi language template</description>
        <protected>false</protected>
        <recipients>
            <recipient>pv_ramana2@hotmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MarketingProductInquiryResponse</template>
    </alerts>
    <fieldUpdates>
        <fullName>update_custom_date</fullName>
        <field>custom_date__c</field>
        <formula>custom_date__c + 1</formula>
        <name>update custom date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>24 hr notice</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Open - Not Contacted,Working - Contacted,Converted &gt; 25k</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>X24_hr_notice</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.custom_date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>custom date change</fullName>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
